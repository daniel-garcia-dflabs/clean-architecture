package com.globant.cleanandroid.login.view

import com.globant.cleanandroid.R
import com.globant.cleanandroid.login.domain.managers.AuthManager
import com.globant.cleanandroid.login.domain.usecases.LoginException
import com.globant.cleanandroid.login.domain.usecases.LoginUseCase
import com.globant.cleanandroid.login.domain.usecases.UserUseCase
import com.globant.cleanandroid.login.model.objects.UserAccountRequest
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val view: LoginContract.View,
                                         private val useCase: LoginUseCase,
                                         private val userUserCase: UserUseCase,
                                         private val authManager: AuthManager) : LoginContract.Presenter {

    override fun login(username: String, password: String) {
        view.hideError()
        view.showLoading(true)
        view.enableLoginButton(false)
        if (!authManager.isLoggedIn()) {
            try {
                view.showLoading(false)
                view.goToDashboard(useCase.execute(UserAccountRequest(username, password)))
            } catch (ex: LoginException) {
                val error = when (ex) {
                    is LoginException.DoesNotExist -> view.getContext().getString(R.string.dialog_error_user_does_not_exist)
                    is LoginException.IncorrectPassword -> view.getContext().getString(R.string.dialog_error_user_password_incorrect)
                }
                view.showLoading(false)
                view.showError(error)
            }
        } else {
            view.showLoading(false)
            view.goToDashboard(userUserCase.getUser())
        }
    }

    override fun validateUsername(username: String?): Boolean {
        //TODO Implement the validators
        return false
    }

    override fun validatePassword(password: String?): Boolean {
        //TODO Implement the validators
        return false
    }

}
