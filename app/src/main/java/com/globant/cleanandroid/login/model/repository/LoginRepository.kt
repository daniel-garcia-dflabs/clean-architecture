package com.globant.cleanandroid.login.model.repository

import com.globant.cleanandroid.login.model.objects.User


interface LoginRepository {

    fun fetchUser(username: String, password: String): User?

}
