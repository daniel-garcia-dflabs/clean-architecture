package com.globant.cleanandroid.login.data

import com.globant.cleanandroid.login.model.objects.User

interface UserRepository {

    interface Local {
        fun getUser(): User
    }

    interface Remote {
        fun fetchUser(username: String, password: String): User
    }

}