package com.globant.cleanandroid.login.view.activities

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import com.globant.cleanandroid.R
import com.globant.cleanandroid.login.model.objects.User
import com.globant.cleanandroid.login.view.LoginContract
import com.globant.commons.mvp.view.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginContract.View {

    private var correctUsernameFlag = false
    private var correctPasswordFlag = false

    @Inject
    lateinit var loginPresenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupLoginButton()
        setupInputFields()
    }

    private fun setupLoginButton() {
        loginButton.setOnClickListener {
            loginPresenter.login(usernameEditText.text.toString().trim(), passwordEditText.text.toString().trim())
        }
    }

    private fun setupInputFields() {
        usernameEditText.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                correctUsernameFlag = loginPresenter.validateUsername(s.toString().trim())
                enableLoginButton(correctPasswordFlag && correctUsernameFlag)
            }
            override fun afterTextChanged(s: Editable?) {}
        })

        passwordEditText.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                correctPasswordFlag = loginPresenter.validatePassword(s.toString().trim())
                enableLoginButton(correctUsernameFlag && correctPasswordFlag)
            }
            override fun afterTextChanged(s: Editable?) {}
        })
    }

    override fun showLoading(show: Boolean) {
        loadingProgressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showError(error: String) {
        errorTextView.text = error
        errorTextView.visibility = View.VISIBLE
    }

    override fun hideError() {
        errorTextView.visibility = View.GONE
    }

    override fun enableLoginButton(enabled: Boolean) {
        loginButton.isEnabled = enabled
    }

    override fun goToPasswordReset() {
        Toast.makeText(getContext(), getString(R.string.act_main_recover_password), Toast.LENGTH_LONG).show()
    }

    override fun goToDashboard(user: User?) {
        //TODO: Navigate to next activity
    }

}
