package com.globant.cleanandroid.login.model.repository

import com.globant.cleanandroid.login.model.objects.LoginResponse

interface LoginApiManager {
    fun login(username: String, password: String): LoginResponse
}
