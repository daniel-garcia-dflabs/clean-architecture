package com.globant.cleanandroid.login.di

import com.globant.cleanandroid.login.view.activities.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector



@Module
abstract class FeatureLoginActivityBuilder {

    @ContributesAndroidInjector(modules = [LoginModule::class])
    internal abstract fun bindLoginActivity(): LoginActivity

}
