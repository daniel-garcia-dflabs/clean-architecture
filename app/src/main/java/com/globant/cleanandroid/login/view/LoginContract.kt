package com.globant.cleanandroid.login.view

import com.globant.cleanandroid.login.model.objects.User
import com.globant.commons.mvp.view.BasePresenter
import com.globant.commons.mvp.view.BaseView

interface LoginContract {

    interface View : BaseView {
        fun showLoading(show: Boolean)
        fun showError(error: String)
        fun hideError()
        fun enableLoginButton(enabled: Boolean)
        fun goToPasswordReset()
        fun goToDashboard(user: User?)
    }

    interface Presenter : BasePresenter {
        fun login(username: String, password: String)
        fun validateUsername(username: String?): Boolean
        fun validatePassword(password: String?): Boolean
    }

}
