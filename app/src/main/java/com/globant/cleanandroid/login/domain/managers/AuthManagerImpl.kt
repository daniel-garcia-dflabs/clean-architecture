package com.globant.cleanandroid.login.domain.managers

import javax.inject.Singleton

@Singleton
class AuthManagerImpl : AuthManager {
    override fun isLoggedIn(): Boolean = true
}