package com.globant.cleanandroid.login.model.objects

data class UserAccountRequest(val username: String, val password: String)