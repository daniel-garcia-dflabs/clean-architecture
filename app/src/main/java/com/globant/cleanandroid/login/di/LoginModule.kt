package com.globant.cleanandroid.login.di

import com.globant.cleanandroid.login.domain.managers.AuthManager
import com.globant.cleanandroid.login.domain.managers.AuthManagerImpl
import com.globant.cleanandroid.login.view.LoginContract
import com.globant.cleanandroid.login.view.LoginPresenter
import com.globant.cleanandroid.login.view.activities.LoginActivity
import dagger.Binds
import dagger.Module


@Module
abstract class LoginModule {

    @Binds
    abstract fun provideLoginView(lobbyActivity: LoginActivity): LoginContract.View

    @Binds
    abstract fun provideLoginPresenter(loginPresenter: LoginPresenter): LoginContract.Presenter

    @Binds
    abstract fun provideAuthManager(authManager: AuthManager): AuthManagerImpl
}