package com.globant.cleanandroid.login.model.objects

data class LoginResponse(val token: String,
                         val id: String,
                         val username: String)
