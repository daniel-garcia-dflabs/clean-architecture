package com.globant.cleanandroid.login.domain.usecases

import com.globant.cleanandroid.login.model.objects.User
import com.globant.cleanandroid.login.model.objects.UserAccountRequest
import com.globant.cleanandroid.login.model.repository.LoginRepository
import com.globant.commons.clean.domain.UseCase
import javax.inject.Inject

sealed class LoginException : kotlin.Exception() {
    class IncorrectPassword : LoginException()
    class DoesNotExist : LoginException()
}

class LoginUseCase @Inject constructor(private val loginRepository: LoginRepository) : UseCase<UserAccountRequest, User>() {

    override fun execute(params: UserAccountRequest): User {
        val user = loginRepository.fetchUser(params.username, params.password)
        return user ?: throw LoginException.DoesNotExist()
    }

}
