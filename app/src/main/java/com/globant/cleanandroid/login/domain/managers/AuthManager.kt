package com.globant.cleanandroid.login.domain.managers

interface AuthManager {
    fun isLoggedIn(): Boolean
}
