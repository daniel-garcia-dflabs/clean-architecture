package com.globant.cleanandroid.internal.di

import android.content.Context
import com.globant.cleanandroid.App
import dagger.Module
import dagger.Provides


/*
    Dagger 2.10
 */
@Module
class AppModule {

    @Provides
    internal fun provideContext(application: App): Context {
        return application.applicationContext
    }

}