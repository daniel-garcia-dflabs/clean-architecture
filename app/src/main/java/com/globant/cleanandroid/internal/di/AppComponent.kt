package com.globant.cleanandroid.internal.di

import com.globant.cleanandroid.App
import com.globant.cleanandroid.login.di.FeatureLoginActivityBuilder
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule


@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    FeatureLoginActivityBuilder::class
])
interface AppComponent : AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}