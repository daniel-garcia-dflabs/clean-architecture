package com.globant.commons.mvp.view


/**
 * Base presenter any presenter of the application must extend.
 * @param V the type of the View the presenter is based on
 * @property view the view the presenter is based on
 */
interface BasePresenter {

    fun onStart() {}

    fun onStop() {}

}