package com.globant.commons.mvp.view

import android.content.Context

interface BaseView {
    fun getContext() : Context
}