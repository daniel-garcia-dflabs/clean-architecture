package com.globant.commons.clean.domain

enum class ThreadMode {
    IO
}

abstract class UseCase<Params, Result> {

    abstract fun execute(params: Params): Result
}